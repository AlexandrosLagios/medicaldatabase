package com.alexandroslagios.daos;

import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.Prescription;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PrescriptionDAO implements EntityDAO<Prescription> {
  @Override
  public Optional<Prescription> get(int id) {
    Optional prescription = null;
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      ResultSet rs = st.executeQuery("SELECT * FROM prescriptions WHERE id=" + id + ";");
    
      if(rs.next())
      {
        prescription = Optional.ofNullable(getFromRS(rs));
      }
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  
    return prescription;
  }
  
  @Override
  public Prescription getFromRS(ResultSet rs) throws SQLException {
    Prescription prescription = new Prescription();
    prescription.setId(rs.getInt("id"));
    prescription.setDateTime(rs.getTimestamp("dateTime"));
    int doctorId = rs.getInt("doctorId");
    DoctorDAO doctorDAO = new DoctorDAO();
    prescription.setDoctor((Doctor) doctorDAO.get(doctorId).get());
    int patientId = rs.getInt("patientId");
    PatientDAO patientDAO = new PatientDAO();
    prescription.setPatient(patientDAO.get(patientId).get());
  
    return prescription;
  }
  
  @Override
  public List<Prescription> getAll() {
    List<Prescription> allPrescriptions = new ArrayList<>();
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      ResultSet rs = st.executeQuery("SELECT * FROM prescriptions;");
    
      while (rs.next()) {
        allPrescriptions.add(getFromRS(rs));
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
  
    return allPrescriptions;  }
  
  @Override
  public void insert(Prescription entity) {
    int id = entity.getId();
    Timestamp dateTime = entity.getDateTime();
    int doctorId = entity.getDoctor().getId();
    int patientId = entity.getPatient().getId();
  
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("INSERT INTO prescriptions VALUES (" + id + ", CURRENT_TIMESTAMP, "
              + doctorId + ", " + patientId + ");");
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  @Override
  public void update(Prescription entity) {
    int id = entity.getId();
    Timestamp dateTime = entity.getDateTime();
    int doctorId = entity.getDoctor().getId();
    int patientId = entity.getPatient().getId();
  
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("UPDATE prescriptions SET dateTime=" + dateTime + ", doctorId=" + doctorId
              + ", patientId=" + patientId + "WHERE id=" + id + ";");
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  @Override
  public void delete(Prescription entity) {
    int id = entity.getId();
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("DELETE FROM prescriptions WHERE id=" + id);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  @Override
  public void delete(int id) {
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("DELETE FROM prescriptions WHERE id=" + id);
    }
    catch (SQLException ex) {
    
      ex.printStackTrace();
    }
  }
}
