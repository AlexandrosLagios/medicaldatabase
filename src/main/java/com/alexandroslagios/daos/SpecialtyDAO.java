package com.alexandroslagios.daos;

import com.alexandroslagios.model.Specialty;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SpecialtyDAO implements EntityDAO<Specialty> {
  
  @Override
  public Optional<Specialty> get(int id) {
    Optional specialty = null;
    try {
      Connection conn = Connector.getConnection();
      Statement stmt = conn.createStatement();
    
      ResultSet rs = stmt.executeQuery("SELECT * FROM specialties WHERE id=" + id);
    
      if(rs.next())
    
      {
        specialty = Optional.ofNullable(getFromRS(rs));
      }
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  
    return specialty;
  }
  
  @Override
  public Specialty getFromRS(ResultSet rs) throws SQLException {
    Specialty specialty = new Specialty();
    specialty.setId(rs.getInt("id"));
    specialty.setName(rs.getString("name"));
    specialty.setDescription(rs.getString("description"));
  
    return specialty;
  }
  
  @Override
  public List<Specialty> getAll() {
    List<Specialty> allSpecialties = new ArrayList<>();
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      ResultSet rs = st.executeQuery("SELECT * FROM specialties;");
    
      while (rs.next()) {
        allSpecialties.add(getFromRS(rs));
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
  
    return allSpecialties;
  }
  
  @Override
  public void insert(Specialty entity) {
    int id = entity.getId();
    String name = entity.getName();
    String description = entity.getDescription();
  
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("INSERT INTO specialties VALUES (" + id + ", '" + name +
              "', '" + description + "');");
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  @Override
  public void update(Specialty entity) {
    int id = entity.getId();
    String name = entity.getName();
    String description = entity.getDescription();
    
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("UPDATE specialties SET name='" + name + "', description='"
              + description + "' WHERE id=" + id + ";");
    }
    catch (SQLException ex) {
    
      ex.printStackTrace();
    }
  }
  
  @Override
  public void delete(Specialty entity) {
    int id = entity.getId();
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("DELETE FROM specialties WHERE id=" + id);
    }
    catch (SQLException ex) {
    
      ex.printStackTrace();
    }
  }
  
  @Override
  public void delete(int id) {
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("DELETE FROM specialties WHERE id=" + id);
    }
    catch (SQLException ex) {
    
      ex.printStackTrace();
    }
  }
}
