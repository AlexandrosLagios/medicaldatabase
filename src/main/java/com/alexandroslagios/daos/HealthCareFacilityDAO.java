package com.alexandroslagios.daos;

import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.HealthCareFacility;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class HealthCareFacilityDAO implements EntityDAO<HealthCareFacility> {
  @Override
  public Optional<HealthCareFacility> get(int id) {
    Optional facility = null;
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      ResultSet rs = st.executeQuery("SELECT * FROM healthCareFacilities WHERE id=" + id);
    
      if(rs.next())
      {
        facility = Optional.ofNullable(getFromRS(rs));
      }
    }
    catch (SQLException ex) {
    
      ex.printStackTrace();
    
    }
  
    return facility;
  }
  
  @Override
  public HealthCareFacility getFromRS(ResultSet rs) throws SQLException {
    HealthCareFacility facility = new HealthCareFacility();
    facility.setId(rs.getInt("id"));
    facility.setName(rs.getString("name"));
    facility.setAddress(rs.getString("address"));
  
    return facility;
  }
  
  @Override
  public List<HealthCareFacility> getAll() {
    List<HealthCareFacility> allFacilities = new ArrayList<>();
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      ResultSet rs = st.executeQuery("SELECT * FROM healthCareFacilities;");
    
      while (rs.next()) {
        allFacilities.add(getFromRS(rs));
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
  
    return allFacilities;
  }
  
  @Override
  public void insert(HealthCareFacility entity) {
    int id = entity.getId();
    String name = entity.getName();
    String address = entity.getAddress();
  
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("INSERT INTO healthCareFacilities VALUES  (" + id + ", '" + name +
              "', '" + address + "');");
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  @Override
  public void update(HealthCareFacility entity) {
    int id = entity.getId();
    String name = entity.getName();
    String address = entity.getAddress();
  
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("UPDATE healthCareFacilities SET name='" + name + "', address='"
              + address + "' WHERE id=" + id + ";");
    }
    catch (SQLException ex) {
    
      ex.printStackTrace();
    }
  }
  
  @Override
  public void delete(HealthCareFacility entity) {
    int id = entity.getId();
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("DELETE FROM healthCareFacilities WHERE id=" + id);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  @Override
  public void delete(int id) {
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("DELETE FROM healthCareFacilities WHERE id=" + id);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  public List<Doctor> getDoctors(int id) {
    List<Doctor> doctors = new ArrayList<>();
    
    try {
      Connection conn = Connector.getConnection();
      String getDoctorsQuery = "SELECT d.* " +
              "FROM doctors d, healthCareFacilities hcf " +
              "WHERE d.healthCareFacilityid = hcf.id " +
              "AND hcf.id = " + id +
              "GROUP BY d.id, d.name, d.specialtyId, d.healthCareFacilityId;";
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(getDoctorsQuery);
      DoctorDAO doctorDAO = new DoctorDAO();
      doctors = doctorDAO.getListFromRS(rs);
      conn.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    
    return doctors;
  }
}
