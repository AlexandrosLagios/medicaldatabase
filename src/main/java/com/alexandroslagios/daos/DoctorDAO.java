package com.alexandroslagios.daos;

import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.Patient;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DoctorDAO implements EntityDAO <Doctor> {
  @Override
  public Optional<Doctor> get(int id) {
    Optional doctor = null;
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      ResultSet rs = st.executeQuery("SELECT * FROM doctors WHERE id=" + id + ";");
  
      if(rs.next())
      {
        doctor = Optional.ofNullable(getFromRS(rs));
      }
      conn.close();
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  
    return doctor;
  }
  
  @Override
  public Doctor getFromRS(ResultSet rs) throws SQLException {
    Doctor doctor = new Doctor();
    doctor.setId(rs.getInt("id"));
    doctor.setName(rs.getString("name"));
    int specialtyId = rs.getInt("specialtyId");
    SpecialtyDAO specialtyDAO = new SpecialtyDAO();
    doctor.setSpecialty(specialtyDAO.get(specialtyId).get());
    int healthCareFacilityId = rs.getInt("healthCareFacilityId");
    HealthCareFacilityDAO healthCareFacilityDAO = new HealthCareFacilityDAO();
    doctor.setHealthCareFacility(healthCareFacilityDAO.get(healthCareFacilityId).get());
    
    return doctor;
  }
  
  public List<Doctor> getListFromRS(ResultSet rs) throws SQLException {
    List<Doctor> doctors = new ArrayList<>();
    while(rs.next())
    {
      Doctor doctor = getFromRS(rs);
      doctors.add(doctor);
    }
    return doctors;
  }
  
  @Override
  public List getAll() {
    List<Doctor> allDoctors = new ArrayList<>();
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
  
      ResultSet rs = st.executeQuery("SELECT * FROM doctors;");
  
      while (rs.next()) {
        allDoctors.add(getFromRS(rs));
      }
      conn.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    
    return allDoctors;
  }
  
  @Override
  public void insert(Doctor entity) {
    int id = entity.getId();
    String name = entity.getName();
    int specialtyId = entity.getSpecialty().getId();
    int healthCareFacilityId = entity.getHealthCareFacility().getId();
  
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("INSERT INTO doctors VALUES (" + id + ", '" + name +
              "', " + specialtyId + ", " + healthCareFacilityId + ");");
      conn.close();
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  @Override
  public void update(Doctor entity) {
    int id = entity.getId();
    String name = entity.getName();
    int specialtyId = entity.getSpecialty().getId();
    int healthCareFacilityId = entity.getHealthCareFacility().getId();
  
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("UPDATE doctors SET name='" + name + "', specialtyId=" + specialtyId
              + ", healthCareFacilityId=" + healthCareFacilityId + "WHERE id=" + id + ";");
      conn.close();
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  
  
  @Override
  public void delete(Doctor entity) {
    int id = entity.getId();
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("DELETE FROM doctors WHERE id=" + id);
      conn.close();
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  @Override
  public void delete(int id) {
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("DELETE FROM doctors WHERE id=" + id);
      conn.close();
    }
    catch (SQLException ex) {
    
      ex.printStackTrace();
    }
  }
  
  public List<Patient> getPatients(int id) {
    List<Patient> patients = new ArrayList<>();
    
    try {
      Connection conn = Connector.getConnection();
      String getPatientsQuery = "SELECT pat.id, pat.name " +
              "FROM patients pat, prescriptions pr, doctors d " +
              "WHERE d.id = pr.doctorId " +
              "AND	pat.id = pr.patientId " +
              "AND d.id = " + id +
              "GROUP BY pat.id, pat.name;";
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(getPatientsQuery);
      PatientDAO patientDAO = new PatientDAO();
      while(rs.next())
      {
        Patient patient = patientDAO.getFromRS(rs);
        patients.add(patient);
      }
      conn.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    
    return patients;
  }
}
