package com.alexandroslagios.daos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * The class with the method getConnection, which establishes a connection with the database.
 */
public class Connector {
  public static final String URL = "jdbc:sqlserver://localhost:1433;instance=SQLEXPRESS;databaseName=MedicalDatabase;";
  public static final String USER = "sa";
  public static final String PASSWORD = "1";
  
  public static Connection getConnection() throws SQLException {
//    System.setProperty("java.net.preferIPv6Addresses", "true");
    Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
    return conn;
  }
}
