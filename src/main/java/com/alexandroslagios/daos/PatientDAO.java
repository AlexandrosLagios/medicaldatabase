package com.alexandroslagios.daos;

import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.Patient;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PatientDAO implements EntityDAO<Patient> {
  @Override
  public Optional<Patient> get(int id) {
    Optional patient = null;
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      ResultSet rs = st.executeQuery("SELECT * FROM patients WHERE id=" + id + ";");
    
      if(rs.next())
      {
        patient = Optional.ofNullable(getFromRS(rs));
      }
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  
    return patient;
  }
  
  @Override
  public Patient getFromRS(ResultSet rs) throws SQLException {
    Patient patient = new Patient();
    patient.setId(rs.getInt("id"));
    patient.setName(rs.getString("name"));
  
    return patient;
  }
  
  @Override
  public List<Patient> getAll() {
    List<Patient> allPatients = new ArrayList<>();
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      ResultSet rs = st.executeQuery("SELECT * FROM patients;");
    
      while (rs.next()) {
        allPatients.add(getFromRS(rs));
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
  
    return allPatients;
  }
  
  @Override
  public void insert(Patient entity) {
    int id = entity.getId();
    String name = entity.getName();
  
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("INSERT INTO patients VALUES (" + id + ", '"
              + name + "');");
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  @Override
  public void update(Patient entity) {
    int id = entity.getId();
    String name = entity.getName();
  
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("UPDATE patients SET name='" + name + "' WHERE id=" + id + ";");
    }
    catch (SQLException ex) {
    
      ex.printStackTrace();
    }
  }
  
  @Override
  public void delete(Patient entity) {
    int id = entity.getId();
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("DELETE FROM patients WHERE id=" + id);
    }
    catch (SQLException ex) {
      ex.printStackTrace();
    }
  }
  
  @Override
  public void delete(int id) {
    try {
      Connection conn = Connector.getConnection();
      Statement st = conn.createStatement();
    
      st.executeUpdate("DELETE FROM patients WHERE id=" + id);
    }
    catch (SQLException ex) {
    
      ex.printStackTrace();
    }
  }
  
  public List<Doctor> getDoctors(int id) {
    List<Doctor> doctors = new ArrayList<>();
  
    try {
      Connection conn = Connector.getConnection();
      String getDoctorsQuery = "SELECT d.* " +
              "FROM patients pat, prescriptions pr, doctors d " +
              "WHERE d.id = pr.doctorId " +
              "AND	pat.id = pr.patientId " +
              "AND d.id = " + id +
              "GROUP BY d.id, d.name, d.specialtyId, d.healthCareFacilityId;";
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(getDoctorsQuery);
      DoctorDAO doctorDAO = new DoctorDAO();
      doctors = doctorDAO.getListFromRS(rs);
      conn.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
  
    return doctors;
  }
}
