package com.alexandroslagios.daos;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class TableEditor {
  public static void createTable(String[] params) throws SQLException {
    Connection conn = Connector.getConnection();
    Statement st = conn.createStatement();
    StringBuilder sqlCreateTable = new StringBuilder();
    sqlCreateTable.append("CREATE TABLE " + params[0] + " (id INT PRIMARY KEY NOT NULL");
    for (int i = 1; i < params.length; i++) {
      sqlCreateTable.append(", " + params[i]);
    }
    sqlCreateTable.append(");");
    st.executeUpdate(sqlCreateTable.toString());
    conn.close();
  }
  
  public static void addColumns(String tableName, String[] columns) throws SQLException {
    Connection conn = Connector.getConnection();
    Statement st = conn.createStatement();
    StringBuilder sqlAlterTable = new StringBuilder();
    sqlAlterTable.append("ALTER TABLE " + tableName + " ADD ");
    sqlAlterTable.append(columns[0]);
    for (int i = 1; i < columns.length; i++) {
      sqlAlterTable.append(", " + columns[i]);
    }
    sqlAlterTable.append(";");
    st.executeUpdate(sqlAlterTable.toString());
    conn.close();
  }
}
