package com.alexandroslagios.daos;

import com.alexandroslagios.model.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * An interface with the main necessary methods to perform CRUD operations
 * on entities.
 * @param <T extends Entity> Any entity of the Medical Database model.
 */
public interface EntityDAO <T extends Entity> {
  /**
   * Returns the entity with the given id, if it exists.
   * @param id int - The id of the entity we want to get.
   * @return Optional<T extends Entity> The entity with the
   * given id,
   * if it exists.
   */
  public Optional<T> get(int id);
  
  public T getFromRS(ResultSet rs) throws SQLException;
  
  /**
   * Returns a list of every entity of the particular type.
   * @return A list of every entity of a particular type.
   */
  public List<T> getAll();
  
  /**
   * Inserts the given entity to a table in the database.
   * @param entity An entity we want to insert to a table.
   */
  public void insert(T entity);
  
  /**
   * Updates some or all of the columns in a row given an entity.
   * @param entity An entity we want to update.
   */
  public void update(T entity);
  
  /**
   * Deletes the row from a table that contains the given entity.
   * @param entity An entity we want to delete from a table in the
   *               database.
   */
  public void delete(T entity);
  
  /**
   * Deletes the row from a table that contains the given entity.
   * @param id The id of an entity we want to delete from a
   *           table in the database.
   */
  public void delete(int id);
}
