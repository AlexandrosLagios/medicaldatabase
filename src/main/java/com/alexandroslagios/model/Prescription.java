package com.alexandroslagios.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * *-1 Doctor
 * *-1 Patient
 * *-* Drug
 * *-* Test
 */
@Data @NoArgsConstructor
public class Prescription extends Entity {
//  private static int idCounter = 0;
  private Timestamp dateTime;
  private Doctor doctor;
  private Patient patient;
  
  public Prescription(int id, Doctor doctor, Patient patient) {
    super(id);
    this.dateTime = new Timestamp(System.currentTimeMillis());
    this.doctor = doctor;
    this.patient = patient;
  }
}
