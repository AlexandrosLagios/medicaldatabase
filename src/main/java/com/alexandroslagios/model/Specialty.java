package com.alexandroslagios.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 1-* Doctor
 */
@Data @NoArgsConstructor
public class Specialty extends Entity {
    private String name;
    private String description;
    
    public Specialty(int id, String name) {
        super(id);
        this.name = name;
    }
}
