package com.alexandroslagios.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * *-* Patient
 * *-1 HealthCareFacility
 * *-1 Specialty
 */
@Data @NoArgsConstructor
public class Doctor extends Entity {
    private String name;
    private Specialty specialty;
    private HealthCareFacility healthCareFacility;
    
    public Doctor(int id, String name, Specialty specialty, HealthCareFacility healthCareFacility) {
        super(id);
        this.name = name;
        this.specialty = specialty;
        this.healthCareFacility = healthCareFacility;
    }
    
    @Override
    public String toString() {
        return "Doctor{ id= " + getId() + ", name='" + name + '\''
                + ", specialty= '" + specialty.getName()
                + "', healthCareFacility= '" + healthCareFacility.getName() + "' }";
    }
}
