package com.alexandroslagios.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * *-* Patient
 */
@Data @NoArgsConstructor
public class HealthCareFacility extends Entity {
    private String name;
    private String address;
    
    public HealthCareFacility(int id, String name, String address) {
        super(id);
        this.name = name;
        this.address = address;
    }
}
