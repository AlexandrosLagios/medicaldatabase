package com.alexandroslagios.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * *-* Doctor
 * 1-* Prescription
 * *-* HealthCareFacility
 */
@Data @NoArgsConstructor
public class Patient extends Entity {
    private String name;
    
    public Patient(int socialSecurityNumber, String name) {
        super(socialSecurityNumber);
        this.name = name;
    }
    
    @Override
    public String toString() {
        return "Patient{ id= " + getId() + ", name= '" + name + "' } ";
    }
}
