package com.alexandroslagios;

import com.alexandroslagios.model.Patient;
import com.alexandroslagios.services.DoctorService;
import com.alexandroslagios.services.HealthCareFacilityService;
import com.alexandroslagios.services.PatientService;
import com.alexandroslagios.tests.DataInitializer;

import java.sql.SQLException;

public class Main {
    
    
    public static void main(String[] args) {
//        try {
//            DataInitializer.initializeTables();
//            DataInitializer.insertSpecialties();
//            DataInitializer.insertFacilities();
//            DataInitializer.insertDoctors();
//            DataInitializer.insertPatients();
//            DataInitializer.insertPrescriptions();

//        }
//        catch (SQLException e) {
//            e.printStackTrace();
//        }
    
        DoctorService doctorService = new DoctorService();
        doctorService.printPatients(2);
    
        PatientService patientService = new PatientService();
        patientService.printDoctors(2);
    
        HealthCareFacilityService healthCareFacilityService = new HealthCareFacilityService();
        healthCareFacilityService.printDoctors(2);
    }
}
