package com.alexandroslagios.tests;

import com.alexandroslagios.daos.*;
import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.HealthCareFacility;
import com.alexandroslagios.model.Specialty;
import com.alexandroslagios.services.DoctorService;
import com.alexandroslagios.services.PatientService;
import com.alexandroslagios.services.PrescriptionService;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DataInitializer {
  public static void initializeTables() throws SQLException {
    Connection conn = Connector.getConnection();
    Statement st = conn.createStatement();
    String[] healthCareFacilityFields = new String[] {"healthCareFacilities", "name VARCHAR(200) NOT NULL", "address " +
            "VARCHAR(300)"};
    String[] specialtyFields = new String[] {"specialties", "name VARCHAR(100)", "description VARCHAR(500)"};
    String[] doctorFields = new String[] {"doctors", "name VARCHAR(100)", "specialtyId INT FOREIGN KEY " +
            "REFERENCES specialties (id)", "healthcareFacilityId INT FOREIGN KEY " +
            "REFERENCES healthCareFacilities (id)"};
    String[] patientFields = new String[] {"patients", "name VARCHAR(100) NOT NULL"};
    String[] prescriptionFields = new String[] {"prescriptions", "dateTime DATETIME2 NOT NULL",
            "doctorId INT FOREIGN KEY REFERENCES doctors (id)", "patientId INT " +
            "FOREIGN KEY REFERENCES patients (id)"};
    TableEditor.createTable(healthCareFacilityFields);
    TableEditor.createTable(specialtyFields);
    TableEditor.createTable(doctorFields);
    TableEditor.createTable(patientFields);
    TableEditor.createTable(prescriptionFields);
    conn.close();
  
  }
  
  public static void insertSpecialties() throws SQLException {
    List<Specialty> specialties = new ArrayList<>();
    specialties.add(new Specialty(1, "General Practice"));
    specialties.add(new Specialty(2, "Pediatrics"));
    specialties.add(new Specialty(3, "Dermatology"));
    
    SpecialtyDAO specialtyDAO = new SpecialtyDAO();
    specialties.forEach(specialty -> specialtyDAO.insert(specialty));
  }
  
  public static void insertFacilities() throws SQLException {
    List<HealthCareFacility> facilities = new ArrayList<>();
    facilities.add(new HealthCareFacility(1, "Sotiria", "afd"));
    facilities.add(new HealthCareFacility(2, "Geniko Kratiko", "lhjp"));
    facilities.add(new HealthCareFacility(3, "Ippokrateio", "fgadfga"));
    facilities.add(new HealthCareFacility(4, "Errikos Dynan", "afafdb"));
  
    HealthCareFacilityDAO healthCareFacilityDAO = new HealthCareFacilityDAO();
    facilities.forEach(healthCareFacility -> healthCareFacilityDAO.insert(healthCareFacility));
  }
  
  public static void insertDoctors() throws SQLException {
    List<Doctor> doctors = new ArrayList<>();
    SpecialtyDAO specialtyDAO = new SpecialtyDAO();
    HealthCareFacilityDAO healthCareFacilityDAO = new HealthCareFacilityDAO();
    
    doctors.add(new Doctor(1, "Sotiris", specialtyDAO.get(2).get(), healthCareFacilityDAO.get(2).get()));
    doctors.add(new Doctor(2, "Nikos", specialtyDAO.get(1).get(), healthCareFacilityDAO.get(3).get()));
    doctors.add(new Doctor(3, "Takis", specialtyDAO.get(3).get(), healthCareFacilityDAO.get(4).get()));
    doctors.add(new Doctor(4, "Vaggelis", specialtyDAO.get(1).get(), healthCareFacilityDAO.get(1).get()));
    doctors.add(new Doctor(5, "Grigoris", specialtyDAO.get(2).get(), healthCareFacilityDAO.get(2).get()));
    doctors.add(new Doctor(6, "Iraklis", specialtyDAO.get(1).get(), healthCareFacilityDAO.get(2).get()));
    doctors.add(new Doctor(7, "Kelly", specialtyDAO.get(1).get(), healthCareFacilityDAO.get(2).get()));
    doctors.add(new Doctor(8, "Phyllis", specialtyDAO.get(1).get(), healthCareFacilityDAO.get(2).get()));
    doctors.add(new Doctor(9, "Pam", specialtyDAO.get(1).get(), healthCareFacilityDAO.get(2).get()));
    
    DoctorDAO doctorDAO = new DoctorDAO();
    doctors.forEach(healthCareFacility -> doctorDAO.insert(healthCareFacility));
  }
  
  public static void insertPatients() throws SQLException {
    PatientService patientService = new PatientService();
    patientService.add(1, "Iason");
    patientService.add(2, "Thrasivoulos");
    patientService.add(3, "Kassandra");
    patientService.add(4, "Marilena");
    patientService.add(5, "Ivy");
    patientService.add(6, "Terkenlis");
    patientService.add(7, "Konstantinidis");
    patientService.add(8, "Kosmikos");
    patientService.add(9, "Despoina");
    patientService.add(10, "Thrasivoulos");
  }
  
  public static void insertPrescriptions() throws SQLException {
    DoctorService doctorService = new DoctorService();
    PatientService patientService = new PatientService();
    PrescriptionService prescriptionService = new PrescriptionService();
    
    prescriptionService.add(1, doctorService.get(4).get(), patientService.get(1).get());
    prescriptionService.add(2, doctorService.get(9).get(), patientService.get(1).get());
    prescriptionService.add(3, doctorService.get(1).get(), patientService.get(6).get());
    prescriptionService.add(4, doctorService.get(8).get(), patientService.get(5).get());
    prescriptionService.add(5, doctorService.get(3).get(), patientService.get(6).get());
    prescriptionService.add(6, doctorService.get(2).get(), patientService.get(10).get());
    prescriptionService.add(7, doctorService.get(1).get(), patientService.get(6).get());
    prescriptionService.add(8, doctorService.get(9).get(), patientService.get(4).get());
    prescriptionService.add(9, doctorService.get(4).get(), patientService.get(3).get());
    prescriptionService.add(10, doctorService.get(3).get(), patientService.get(10).get());
    prescriptionService.add(11, doctorService.get(2).get(), patientService.get(2).get());
    prescriptionService.add(12, doctorService.get(2).get(), patientService.get(3).get());
    prescriptionService.add(13, doctorService.get(1).get(), patientService.get(5).get());
    prescriptionService.add(14, doctorService.get(5).get(), patientService.get(1).get());
    prescriptionService.add(15, doctorService.get(8).get(), patientService.get(2).get());
    prescriptionService.add(16, doctorService.get(9).get(), patientService.get(4).get());
    prescriptionService.add(17, doctorService.get(5).get(), patientService.get(3).get());
    prescriptionService.add(18, doctorService.get(4).get(), patientService.get(10).get());
  }
}
