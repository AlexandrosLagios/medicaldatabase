package com.alexandroslagios.Unused;

import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.HealthCareFacility;
import lombok.Data;

import java.util.List;

/**
 * 1-* Doctor
 * *-* Patient
 */
@Data
public class Clinic extends HealthCareFacility {
    private List<Doctor> doctors;
}
