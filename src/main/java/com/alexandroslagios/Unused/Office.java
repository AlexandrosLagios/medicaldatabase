package com.alexandroslagios.Unused;

import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.HealthCareFacility;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 1-1 Doctor
 * *-* Patient
 */
@Data @AllArgsConstructor
public class Office extends HealthCareFacility {
    private Doctor doctor;
}
