package com.alexandroslagios.Unused;

import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.HealthCareFacility;
import lombok.Data;

import java.util.List;

/**
 * 1-* with Doctor
 * *-* with Patient
 */
@Data
public class Hospital extends HealthCareFacility {
    List<Doctor> doctors;
}
