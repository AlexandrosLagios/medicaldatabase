package com.alexandroslagios.services;

import com.alexandroslagios.daos.HealthCareFacilityDAO;
import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.HealthCareFacility;
import com.alexandroslagios.model.Patient;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class HealthCareFacilityService implements EntityService<HealthCareFacility> {
  
  HealthCareFacilityDAO healthCareFacilityDAO;
  
  public HealthCareFacilityService() {
    this.healthCareFacilityDAO = new HealthCareFacilityDAO();
  }
  
  public void add(int id, String name, String address) {
    HealthCareFacility facility = new HealthCareFacility(id, name, address);
    healthCareFacilityDAO.insert(facility);
  }
  
  @Override
  public void add(HealthCareFacility entity) {
    healthCareFacilityDAO.insert(entity);
  }
  
  @Override
  public Optional<HealthCareFacility> get(int id) {
    return healthCareFacilityDAO.get(id);
  }
  
  @Override
  public List<HealthCareFacility> getAll() {
    return healthCareFacilityDAO.getAll();
  }
  
  @Override
  public void remove(HealthCareFacility entity) {
    healthCareFacilityDAO.delete(entity);
  }
  
  @Override
  public void remove(int id) {
    healthCareFacilityDAO.delete(id);
  }
  
  /**
   * Prints the information of the doctors that work for the facility with the
   * given id
   * @param id The id of a health care facility.
   */
  public void printDoctors(int id) {
    List<Doctor> doctors = healthCareFacilityDAO.getDoctors(id);
    String facilityName = healthCareFacilityDAO.get(id).get().getName();
    if (doctors.isEmpty()) {
      System.out.println("Facility " + facilityName + " does not employ any doctors.");
      return;
    }
  
    System.out.println("Facility " + facilityName + " employs the following doctor(s): \n");
    healthCareFacilityDAO.getDoctors(id).stream()
            .forEach(doctor -> System.out.println(doctor.toString()));
    System.out.println();
  }
}
