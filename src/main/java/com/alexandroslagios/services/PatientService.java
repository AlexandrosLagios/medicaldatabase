package com.alexandroslagios.services;

import com.alexandroslagios.daos.PatientDAO;
import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.Patient;

import java.util.List;
import java.util.Optional;

public class PatientService implements EntityService<Patient> {
  
  PatientDAO patientDAO;
  
  public PatientService() {
    this.patientDAO = new PatientDAO();
  }
  
  public void add(int id, String name) {
    Patient patient = new Patient(id, name);
    patientDAO.insert(patient);
  }
  
  @Override
  public void add(Patient entity) {
    patientDAO.insert(entity);
  }
  
  @Override
  public Optional<Patient> get(int id) {
    return patientDAO.get(id);
  }
  
  @Override
  public List<Patient> getAll() {
    return patientDAO.getAll();
  }
  
  @Override
  public void remove(Patient entity) {
    patientDAO.delete(entity);
  }
  
  @Override
  public void remove(int id) {
    patientDAO.delete(id);
  }
  
  /**
   * Prints the information of every doctor that has written prescriptions to
   * the patient with the given id.
   * @param id The id of a doctor.
   */
  public void printDoctors(int id) {
    List<Doctor> doctors = patientDAO.getDoctors(id);
    String patientName = patientDAO.get(id).get().getName();
    if (doctors.isEmpty()) {
      System.out.println("Patient " + patientName + " has not received any prescriptions");
      return;
    }
    
    System.out.println("Patient " + patientName + " has received prescriptions from the following doctor(s): \n");
    patientDAO.getDoctors(id).stream()
            .forEach(doctor -> System.out.println(doctor.toString()));
    System.out.println();
  }
}
