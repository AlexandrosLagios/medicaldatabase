package com.alexandroslagios.services;

import com.alexandroslagios.daos.PrescriptionDAO;
import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.Patient;
import com.alexandroslagios.model.Prescription;

import java.util.List;
import java.util.Optional;

public class PrescriptionService implements EntityService<Prescription> {
  PrescriptionDAO prescriptionDAO;
  
  public PrescriptionService() {
    this.prescriptionDAO = new PrescriptionDAO();
  }
  
  public void add(int id, Doctor doctor, Patient patient) {
    Prescription prescription = new Prescription(id, doctor, patient);
    prescriptionDAO.insert(prescription);
  }
  
  @Override
  public void add(Prescription entity) {
    prescriptionDAO.insert(entity);
  }
  
  @Override
  public Optional<Prescription> get(int id) {
    return prescriptionDAO.get(id);
  }
  
  @Override
  public List<Prescription> getAll() {
    return prescriptionDAO.getAll();
  }
  
  @Override
  public void remove(Prescription entity) {
    prescriptionDAO.delete(entity);
  }
  
  @Override
  public void remove(int id) {
    prescriptionDAO.delete(id);
  }
}
