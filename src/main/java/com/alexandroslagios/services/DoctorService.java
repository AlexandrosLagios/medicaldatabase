package com.alexandroslagios.services;

import com.alexandroslagios.daos.DoctorDAO;
import com.alexandroslagios.model.Doctor;
import com.alexandroslagios.model.HealthCareFacility;
import com.alexandroslagios.model.Patient;
import com.alexandroslagios.model.Specialty;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DoctorService implements EntityService<Doctor>{
  
  DoctorDAO doctorDAO;
  
  public DoctorService() {
    this.doctorDAO = new DoctorDAO();
  }
  
  public void add(int id, String name, Specialty specialty, HealthCareFacility healthCareFacility) {
    Doctor doctor = new Doctor(id, name, specialty, healthCareFacility);
    doctorDAO.insert(doctor);
  }
  
  @Override
  public void add(Doctor entity) {
    doctorDAO.insert(entity);
  }
  
  @Override
  public Optional<Doctor> get(int id) {
    return doctorDAO.get(id);
  }
  
  @Override
  public List<Doctor> getAll() {
    return doctorDAO.getAll();
  }
  
  @Override
  public void remove(Doctor entity) {
    doctorDAO.delete(entity);
  }
  
  @Override
  public void remove(int id) {
    doctorDAO.delete(id);
  }
  
  /**
   * Prints the information of the patients whom the doctor with the given id
   * has written prescriptions to.
   * @param id The id of a doctor.
   */
  public void printPatients(int id) {
    List<Patient> patients = new ArrayList<>();
    String doctorName = doctorDAO.get(id).get().getName();
    if (patients.isEmpty()) {
      System.out.println("Doctor " + doctorName + " has not written any prescriptions.");
    }
    
    System.out.println("Doctor " + doctorName + " has written prescriptions for the following patient(s): \n");
    doctorDAO.getPatients(id).stream()
            .forEach(patient -> System.out.println(patient.toString()));
    System.out.println();
  }
}
