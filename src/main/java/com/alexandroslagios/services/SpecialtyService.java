package com.alexandroslagios.services;

import com.alexandroslagios.daos.SpecialtyDAO;
import com.alexandroslagios.model.Specialty;

import java.util.List;
import java.util.Optional;

public class SpecialtyService implements EntityService<Specialty> {
  
  SpecialtyDAO specialtyDAO;
  
  public SpecialtyService() {
    this.specialtyDAO = new SpecialtyDAO();
  }
  
  public void add(int id, String name) {
    Specialty specialty = new Specialty(id, name);
    specialtyDAO.insert(specialty);
  }
  
  @Override
  public void add(Specialty entity) {
    specialtyDAO.insert(entity);
  }
  
  @Override
  public Optional<Specialty> get(int id) {
    return specialtyDAO.get(id);
  }
  
  @Override
  public List<Specialty> getAll() {
    return specialtyDAO.getAll();
  }
  
  @Override
  public void remove(Specialty entity) {
    specialtyDAO.delete(entity);
  }
  
  @Override
  public void remove(int id) {
    specialtyDAO.delete(id);
  }
}
