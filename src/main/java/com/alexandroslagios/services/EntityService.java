package com.alexandroslagios.services;

import com.alexandroslagios.model.Entity;

import java.util.List;
import java.util.Optional;

public interface EntityService <T extends Entity> {
  public void add(T entity);
  
  public Optional<T> get(int id);
  
  public List<T> getAll();
  
  public void remove(T entity);
  
  public void remove(int id);
}
